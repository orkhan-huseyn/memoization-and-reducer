import { DECREMENT, INCREMENT, RESET } from './actions';

export function counterReducer(state, action) {
  switch (action.type) {
    case INCREMENT:
      return {
        counter: state.counter + action.payload,
      };
    case DECREMENT:
      return {
        counter: state.counter - 1,
      };
    case RESET:
      return {
        counter: 0,
      };
    default:
      return state;
  }
}
