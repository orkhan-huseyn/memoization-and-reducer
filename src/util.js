const memo = {};

function fib(n) {
  if (n in memo) {
    return memo[n];
  }

  if (n < 1) return 1;

  const result = fib(n - 1) + fib(n - 2);
  memo[n] = result;

  return result;
}

export function heavyCalculation(n) {
  let sum = n;
  for (let i = 1; i < 1e9; i++) {
    sum += i;
  }
  return sum;
}
