import { useEffect, useState, useCallback } from 'react';
import Dummy from './Dummy';

function CallbackExample() {
  const [posts, setPosts] = useState([]);
  const [skip, setSkip] = useState(0);

  const getPosts = useCallback(async () => {
    const response = await fetch('https://dummyjson.com/posts?skip=' + skip);
    const json = await response.json();
    setPosts(json.posts);
  }, []);

  useEffect(() => {
    getPosts();
  }, []);

  return (
    <>
      <h1>Skip {skip}</h1>
      <button onClick={() => setSkip((c) => c + 1)}>Increment</button>
      <button onClick={getPosts}>refresh posts</button>
      <Dummy getPosts={getPosts} />
    </>
  );
}

export default CallbackExample;
