import { useReducer } from 'react';
import { decrement, increment, reset } from './actions';
import { counterReducer } from './reducer';

const initialState = {
  counter: 0,
};

function App() {
  const [state, dispatch] = useReducer(counterReducer, initialState);
  return (
    <>
      <h1>Counter: {state.counter}</h1>
      <button onClick={() => dispatch(increment())}>Increment</button>
      <button onClick={() => dispatch(increment(5))}>Increment by 5</button>
      <button onClick={() => dispatch(decrement())}>Decrement</button>
      <button onClick={() => dispatch(reset())}>Reset</button>
    </>
  );
}

export default App;
