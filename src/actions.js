// action type
export const INCREMENT = 'increment';
export const DECREMENT = 'decrement';
export const RESET = 'reset';

// action creator
export function increment(by = 1) {
  // action
  return {
    type: INCREMENT,
    payload: by
  };
}

export function decrement() {
  return {
    type: DECREMENT,
  };
}

export function reset() {
  return {
    type: RESET,
  };
}
