import React from 'react';

function Dummy() {
  console.log('Dummy function called!');
  return <h1>I am dummy</h1>;
}

export default React.memo(Dummy);
