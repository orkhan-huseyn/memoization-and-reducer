import { useState, useMemo } from 'react';
import { heavyCalculation } from './util';

function MemoExample() {
  const [count, setCount] = useState(0);
  const [todos, setTodos] = useState([]);

  const value = useMemo(() => heavyCalculation(count), [count]);

  function incrementCount() {
    setCount((c) => c + 1);
  }

  function addTodo() {
    setTodos((t) => [...t, 'New todo']);
  }

  return (
    <>
      <h1>Counter is: {count}</h1>
      <button onClick={incrementCount}>Increment</button>
      <span>Calculated value: {value}</span>
      <hr />
      <ul>
        {todos.map((todo, index) => (
          <li key={index}>{todo}</li>
        ))}
      </ul>
      <button onClick={addTodo}>Add new todo</button>
    </>
  );
}

export default MemoExample;
